package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers == null || inputNumbers.size() < 3) {
            throw new CannotBuildPyramidException();
        }

        for (Integer item : inputNumbers) {
            if (item == null) {
                throw new CannotBuildPyramidException();
            }
        }

        Set<Integer> resultSet = new TreeSet<>();

        for (Integer item : inputNumbers){
            resultSet.add(item);
        }

        boolean differentSize = inputNumbers.size() == resultSet.size();

        if (!differentSize){
            throw new CannotBuildPyramidException();
        }else{
            Collections.sort(inputNumbers);
        }


        int[][] pyramid = createBasePyramid(inputNumbers);

        if (inputNumbers.size() == 3) {
            return pyramid;
        }

        int index = 3;

        while (index < inputNumbers.size()) {

            pyramid = expandPyramid(pyramid);

            for (int i = 0; i < pyramid[pyramid.length - 1].length; i++) {

                if (i % 2 == 0) {
                    pyramid[pyramid.length - 1][i] = inputNumbers.get(index);
                    index++;
                    if (index == inputNumbers.size()) {
                        break;
                    }
                } else {
                    pyramid[pyramid.length - 1][i] = 0;
                }
            }
        }

        boolean checked = pyramidCheck(pyramid);

        if (!checked) {
            throw new CannotBuildPyramidException();
        }

        return pyramid;
    }

    private int[][] createBasePyramid(List<Integer> inputNumbers) {

        int[][] basePyramid = new int[2][3];

        basePyramid[0][0] = 0;
        basePyramid[0][1] = inputNumbers.get(0);
        basePyramid[0][2] = 0;
        basePyramid[1][0] = inputNumbers.get(1);
        basePyramid[1][1] = 0;
        basePyramid[1][2] = inputNumbers.get(2);

        return basePyramid;
    }

    private int[][] expandPyramid(int[][] pyramid) {

        int[][] newPyramid = new int[pyramid.length + 1][pyramid[0].length + 2];

        for (int i = 0; i <= newPyramid.length - 2; i++) {

            int[] newRow = new int[pyramid[0].length + 2];

            newRow[0] = 0;
            System.arraycopy(pyramid[i], 0, newRow, 1, pyramid[i].length);
            newRow[newRow.length - 1] = 0;

            newPyramid[i] = newRow;
        }
        return newPyramid;
    }

    private boolean pyramidCheck(int[][] pyramid) {

        int[] lastRow = pyramid[pyramid.length - 1];
        int lastItem = lastRow[lastRow.length - 1];

        return lastItem != 0;
    }


}

//public boolean findDuplicates(List<Integer> listContainingDuplicates){
//
//    Set<Integer> resultSet = new TreeSet<>();
//
//    for (Integer item : listContainingDuplicates){
//        resultSet.add(item);
//    }
//
//    boolean differentSize = listContainingDuplicates.size() == resultSet.size();
//
//    if (!differentSize){
//        throw new CannotBuildPyramidException();
//    }else return true;
//}

